Key,Source,Context,English
ATBerryTeaDesc,items,Food,A warm healing tea made from blueberries

ATMixedTeaDesc,items,Food,A cool refreshing mixed herbal tea that helps protect against the heat

ATIcedCoffeeDesc,items,Food,A cool energising iced coffee that helps protect against the heat whilst remaining just as hydrating as the hot counterpart

ATYuccaChipsDesc,items,Food,A handful of hearty wedge cut yucca chips

ATHashbrownDesc,items,Food,A small triangle of shredded fried potato. A nice snack

ATMashPotatoDesc,items,Food,This hefty pot of silky smooth mash potato could be a meal on it's own

ATCornGritsDesc,items,Food,A warm pot of boiled corn grits with animal fat

ATBlueberryJamDesc,items,Food,Sweet blueberry jam made from rendered fruits. Maybe too sweet on it's own

ATYuccaJamDesc,items,Food,Surprisingly savory whilst still being sweet. Yucca jam is made from rendered down yucca fruit

ATEggSaladDesc,items,Food,A basic egg salad made from boiled eggs. Boiled potato and oil dressing

ATFriedMeatDesc,items,Food,Meat that has been fried in a cornmeal coating

ATCornChipsDesc,items,Food,Crispy fried corn chips that make for a nice snack between bashing skulls in

ATBBJamSandwichDesc,items,Food,A simple bread sandwich with a sweet blueberry jam filling to help recover from a long day

ATYuccaJamSandwichDesc,items,Food,A simple bread sandwich with a savory yucca jam filling that helps get rid of even the strongest hunger pangs

ATCoffeeCakeDesc,items,Food,This cake offers the energising properties of coffee whilst still being delicious

ATMushroomPieDesc,items,Food,Savory and buttery pie crust filled to the brim with mushrooms

ATMeatAndBeerPieDesc,items,Food,Rich and savory pie crust with a meat and beer filling. Truly a feast fit for the best of the best

ATFriedMushroomsDesc,items,Food,Whole mushrooms coated in cornmeal and fried. A very filling snack

ATFriedEggDesc,items,Food,A simple sunny side up fried egg. The main component of any good breakfast

ATFrenchToastDesc,items,Food,Egg soaked bread that has been fried. Simple yet tasty

ATHoneyGlazedMeatDesc,items,Food,Roasted meat with a deep honey glaze covering it

ATBlueberrySodaDesc,items,Food,A refreshing carbonated soda made from blueberries

ATCarbonatedWaterDesc,items,Food,Carbonated water that is both refreshing and useful for digestion

ATKvassSodaDesc,items,Food,Kvass soda made from fermented cornbread to make it carbonated. A very basic soda

ATVodkaDesc,items,Food,Fermented and distilled potato water that will improve your bartering. This stuff packs a punch


ATBlueberrySpongeDesc,items,Food,Fluffy sponge cake with a very sweet blueberry jam filling. Filling for both the body and soul.

ATYuccaSpongeDesc,items,Food,Sweet and fluffy sponge cake with a somewhat savory yucca jam filling. Extremely filling

ATCustardPieDesc,items,Food,Buttery pie crust filled to the brim with a dense custard.

ATEnergyTabletDesc,items,Food,Energy tablets are great for getting you out of sticky situations. However you can get addicted to them.

ATFudgeDesc,items,Food,Sugary fudge that melts in the mouth and rots away the teeth. You didn't need those molars anyway

ATPopcornDesc,items,Food,Popped corn - the movie-goers favourite. A basic snack between meals

ATRockCandyDesc,items,Food,Unflavoured candy made mostly from sugar. Will give you quite the sugar rush

ATRockCandyBlueberryDesc,items,Food,Blueberry flavoured rock candy that tastes good and will give you a sugar rush


ATSpongeCakeDesc,items,Food,Dense sponge cake with sugar icing atop it.

ATSugarExtractDesc,items,Food,You'd have to be pretty desperate to eat raw sugar. Used in soda, candies and cakes

ATVodkaDrunkDesc,buffs,Buff,That stuff burns. You feel more talkative

ATSugarRushDesc,buffs,Buff,A slight rush from the amount of sugar you just ate. This allows you to slightly run faster
ATSugarRushName,buffs,Buff,Sugar Rush
ATSugarRushTooltip,buffs,Buff,That was a lot of sugar.

ATEnergyTabletDesc,buffs,Buff,You can run faster. reload slightly quicker and become less exhausted.
ATEnergyTabletName,buffs,Buff,Energy Rush
ATEnergyTabletTooltip,buffs,Buff,What a rush!

ATColdDrinkName,buffs,Buff,Cold Drink
ATColdDrinkDesc,buffs,Buff,Iced drinks help you survive better in hot environments.

drinkBerryTea,items,Food,Berry Tea
drinkBerryTeaDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
drinkBerryTeaSchematic,items,Food,Berry Tea Recipe

drinkIcedMixedTea,items,Food,Iced Mixed tea
drinkIcedMixedTeaDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
drinkIcedMixedTeaSchematic,items,Food,Iced Mixed tea Recipe

foodYuccaChips,items,Food,Yucca Chips
foodYuccaChipsDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodYuccaChipsSchematic,items,Food,Yucca Chips Recipe

foodHashbrown,items,Food,Hashbrown
foodHashbrownDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodHashbrownSchematic,items,Food,Hashbrown Recipe

foodMashedPotato,items,Food,Mashed Potato
foodMashedPotatoDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodMashedPotatoSchematic,items,Food,Mashed Potato Recipe

foodBlueberryJam,items,Food,Blueberry Jam
foodBlueberryJamDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodBlueberryJamSchematic,items,Food,Blueberry Jam Recipe

foodYuccaJam,items,Food,Yucca Jam
foodYuccaJamDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodYuccaJamSchematic,items,Food,Yucca Jam Recipe

foodEggSalad,items,Food,Egg Salad
foodEggSaladDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodEggSaladSchematic,items,Food,Egg Salad Recipe

foodFriedMeat,items,Food,Fried Meat
foodFriedMeatDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodFriedMeatSchematic,items,Food,Fried Meat Recipe

foodCornChips,items,Food,Corn Chips
foodCornChipsDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodCornChipsSchematic,items,Food,Corn Chips Recipe

foodBlueberryJamSandwich,items,Food,Blueberry Sandwich
foodBlueberryJamSandwichDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodBlueberryJamSandwichSchematic,items,Food,Blueberry Sandwich Recipe

foodYuccaJamSandwich,items,Food,Yucca Sandwich
foodYuccaJamSandwichDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodYuccaJamSandwichSchematic,items,Food,Yucca Sandwich Recipe

foodCoffeeCake,items,Food,Coffee Cake
foodCoffeeCakeDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodCoffeeCakeSchematic,items,Food,Coffee Cake Recipe

foodMushroomPie,items,Food,Mushroom Pie
foodMushroomPieDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodMushroomPieSchematic,items,Food,Mushroom Pie Recipe

foodMeatAndBeerPie,items,Food,Meat and Beer Pie
foodMeatAndBeerPieDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodMeatAndBeerPieSchematic,items,Food,Meat and Beer Pie Recipe

foodFriedMushrooms,items,Food,Fried Mushrooms
foodFriedMushroomsDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodFriedMushroomsSchematic,items,Food,Fried Mushrooms Recipe

foodFriedEgg,items,Food,Fried Egg
foodFriedEggDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodFriedEggSchematic,items,Food,Fried Egg Recipe

foodFrenchToast,items,Food,French Toast
foodFrenchToastDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodFrenchToastSchematic,items,Food,French Toast Recipe

foodHoneyGlazedMeat,items,Food,Honey Glazed Meat
foodHoneyGlazedMeatDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodHoneyGlazedMeatSchematic,items,Food,Honey Glazed Meat Recipe

drinkBlueberrySoda,items,Food,Blueberry Soda
drinkBlueberrySodaDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
drinkBlueberrySodaSchematic,items,Food,Blueberry Soda Recipe

drinkKvassSoda,items,Food,Kvass Soda
drinkKvassSodaDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
drinkKvassSodaSchematic,items,Food,Kvass Soda Recipe

drinkCarbonatedWater,items,Food,Carbonated Water
drinkCarbonatedWaterDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
drinkCarbonatedWaterSchematic,items,Food,Carbonated Water Recipe

drinkDistilledVodka,items,Food,Distilled Vodka
drinkDistilledVodkaDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
drinkDistilledVodkaSchematic,items,Food,Distilled Vodka Recipe


foodSugarExtract,items,Food,Sugar Extract
foodSugarExtractDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodSugarExtractSchematic,items,Food,Sugar Extract Recipe

foodRockCandy,items,Food,Rock Candy
foodRockCandyDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodRockCandySchematic,items,Food,Rock Candy Recipe

foodBlueberryCandy,items,Food,Blueberry Candy
foodBlueberryCandyDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodBlueberryCandySchematic,items,Food,Blueberry Candy Recipe


foodPlainFudge,items,Food,Plain Fudge
foodPlainFudgeDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodPlainFudgeSchematic,items,Food,Plain Fudge Recipe

medicalEnergyTablet,items,Food,Energy Tablet
medicalEnergyTabletDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
medicalEnergyTabletSchematic,items,Food,Energy Tablet Recipe

foodSpongeCake,items,Food,Sponge Cake
foodSpongeCakeDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodSpongeCakeSchematic,items,Food,Sponge Cake Recipe

foodBlueberrySpongeCake,items,Food,Blueberry Sponge Cake
foodBlueberrySpongeCakeDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodBlueberrySpongeCakeSchematic,items,Food,Blueberry Sponge Cake Recipe

foodYuccaSpongeCake,items,Food,Yucca Sponge Cake
foodYuccaSpongeCakeDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodYuccaSpongeCakeSchematic,items,Food,Yucca Sponge Cake Recipe

foodCustardPie,items,Food,Custard Pie
foodCustardPieDesc,items,Food,Cooking in the Apocalypse is extremely important to survival.
foodCustardPieSchematic,items,Food,Custard Pie Recipe


drinkHealthDrink,items,Food,Health Drink
drinkHealthDrinkDesc,items,Food,Drink This and Gain Your Health and Energy Back!
drinkHealthDrinkSchematic,items,Food,Health Drink Recipe

foodHamburger,items,Food,Hamburger
foodHamburgerDesc,items,Food,It Has a good smell and looks tasty.
foodHamburgerSchematic,items,Food,Hamburger Recipe

foodBaconEggSandwich,items,Food,Bacon and Egg Sandwich
foodBaconEggSandwichDesc,items,Food,You made this good looking sandwich.
foodBaconEggSandwichSchematic,items,Food,Bacon and Egg Sandwich Recipe

foodFishPie,items,Food,Fish Pie
foodFishPieDesc,items,Food,The Can of Tuna Maybe rotten but you did a good job making this food!
foodFishPieSchematic,items,Food,Fish Pie Recipe

foodHoneyHam,items,Food,Honey Ham
foodHoneyHamDesc,items,Food,MmmHH Very good meal.
foodHoneyHamSchematic,items,Food,Honey Ham Recipe

foodGrillSteak,items,Food,Grilled Steak
foodGrillSteakDesc,items,Food,Better then Grilled meat.
foodGrillSteakSchematic,items,Food,Grilled Steak Recipe

foodGrillSteakSandwich,items,Food,Grilled Steak Sandwich
foodGrillSteakSandwichDesc,items,Food,You Made the Steak Even Better!
foodGrillSteakSandwichSchematic,items,Food,Grilled Steak Sandwich Recipe

foodBiscuit,items,Food,Biscuit
foodBiscuitDesc,items,Food,Biscuits are delicious by themselves but their best use is spreading some jam on them.
foodBiscuitSchematic,items,Food,Biscuit Recipe

foodBiscuitWithJam,items,Food,Biscuit With Jam
foodBiscuitWithJamDesc,items,Food,Handmade biscuit smeared with a nice layer of blueberry jam.
foodBiscuitWithJamSchematic,items,Food,Biscuit With Jam Recipe

foodRamenSoup,items,Food,Ramen Soup
foodRamenSoupDesc,items,Food,Nice you made a bowl of Ramen Soup Not the best if you ask me.
foodRamenSoupSchematic,items,Food,Ramen Soup Recipe

foodTunaSandwichRH,items,Food,Tuna Sandwich
foodTunaSandwichRHDesc,items,Food,Not the best sandwich but eatable.
foodTunaSandwichRHSchematic,items,Food,Tuna Sandwich Recipe

foodMysteryMeat,items,Food,Mystery Meat
foodMysteryMeatDesc,items,Food,It doesn't smell terrible but you don't know how much faith you should put in Hotrod.
foodMysteryMeatSchematic,items,Food,Mystery Meat Recipe

foodMysteryMeatCrafted,items,Food,Mystery Meat
foodMysteryMeatCraftedDesc,items,Food,It doesn't smell terrible but you don't know how much faith you should put in Hotrod.
foodMysteryMeatCraftedSchematic,items,Food,Mystery Meat Recipe

foodCandyBar,items,Food,Candy Bar
foodCandyBarDesc,items,Food,Diabeetus what diabeetus. This is the apocalypse so have at it and shove that candy in your mouth like there is no tomorrow!

foodGoldfish,items,Food,Goldfish
foodGoldfishDesc,items,Food,The snack that smiles back!? SCORE.

foodRamenNoodles,items,Food,Ramen Noodles
foodRamenNoodlesDesc,items,Food,You Can make this into a soup or you can eat it.

foodBagOfChips,items,Food,Bag Of Chips
foodBagOfChipsDesc,items,Food,Bet you can not eat just one!

foodBagOfHotChips,items,Food,Bag Of Hot Chips
foodBagOfHotChipsDesc,items,Food,Spicy goodness!


qc_hotrod,items,Food,Hotrod Challenge
qc_Feedthemole,items,Food,Feed The Mole Challenge
qc_Sandwich,items,Food,Sandwich Challenge


peculiarTasteNAME,buffs,Buff,Peculiar Taste



breakfastBook,items,Book,Home Cooking Vol 1: Knights Morning Meats,,,,,
dinnerBook,items,Book,Home Cooking Vol 2: Wookies Eats,,,,,
stewsBook,items,Book,Home Cooking Vol 3: Sinders Stews,,,,,
bakingBook,items,Book,Home Cooking Vol 4: Fallens Baked Goods,,,,,
drinksBook,items,Book,Home Cooking Vol 5: Tea Time With Shorty,,,,,
breakfastBookDesc,items,Book,"Unlocks breakfast related recipes.",,,,
dinnerBookDesc,items,Book,"Unlocks dinner and sides related recipes.",,,,
stewsBookDesc,items,Book,"Unlocks delicious hearty stews.",,,,
bakingBookDesc,items,Book,"Unlocks baked goods and supplies.",,,,
drinksBookDesc,items,Book,"Unlocks drinks and teas.",,,,
sandwichBook,items,Book,Home Cooking Vol 6: Shades Mouth Watering Sandwiches,,,,,
sandwichBookDesc,items,Book,"Unlocks sandwich related recipes.",,,,
foodSmokerSchematic,items,Book,Home Smoker Foods Schematic,,,,,
foodEggBaconSandwichSchematic,items,Book,Bacon Egg On A Hero Schematic,,,,,
foodSoupMushroomSchematic,items,Book,Mushroom Soup Schematic,,,,,
foodVinegarSchematic,items,Book,Vinegar Schematic,,,,,


foodVinegar,items,Food,Vinegar,,,,,
foodMayonnaise,items,Food,Mayonnaise,,,,,
foodBrisket,items,Food,Brisket,,,,,
foodPulledPorkSandwich,items,Food,Pulled Pork Sandwich,,,,,
foodPulledPorkSandwichDesc,items,Food,Never eat something this good on a Blood Moon.,,,,,
foodPulledPork,items,Food,Pulled Pork,,,,,
foodBeerCanChicken,items,Food,Beer Can Chicken,,,,,
foodSushi,items,Food,Sushi,,,,,
foodPickles,items,Food,Pickles,,,,,
foodColeSlaw,items,Food,Cole Slaw,,,,,
foodFriedBacon,items,Food,Fried Bacon,,,,,
foodFriedBaconDesc,items,Food,It does not get any better.,,,,,
foodFreshSteak,items,Food,Fresh Steak,,,,,
foodFreshSmokedSausage,items,Food,Fresh Smoked Sausage,,,,,
foodFreshBBQChickenLegs,items,Food,Fresh BBQ Chicken Legs,,,,,
foodDeulxeHamCheeseSandwich,items,Food,Ham and Cheese Deluxe,,,,,
foodChickenBaconSandwich,items,Food,Chicken and Bacon Sandwich,,,,,
foodEggBaconSandwich,items,Food,Bacon Egg On A Hero,,,,,
foodBakedPotatoSandwich,items,Food,Baked Potato Sandwich,,,,,
foodDeluxeFishSandwich,items,Food,Fish Deluxe,,,,,
foodShadesDanishFishDish,items,Food,Shades Danish Fish Dish,,,,,
foodTunaBLTSandwich,items,Food,Tuna BLT,,,,,
resourceDough,items,Food,Dough,,,,,
resourceDoughDesc,items,Food,Ingredient used in pasta and breads.,,,,,
foodCheese,items,Food,Cheese,,,,,
drinkMilk,items,Food,Milk,,,,,
drinkMilkDesc,items,Food,"Holy balls! Milk? Still? Better check that expiration date pal!",,,,,
foodCreamDesc,items,Food,Delicious cream can make anything better. Or just drink it moose.,,,,,

foodBoiledCabbageRH,items,Food,Boiled Cabbage,,,,,
foodBoiledCabbageRHDesc,items,Food,An Irish tradition and a great way to warm a survivors heart.,,,,,
foodOmelet,items,Food,Omelet,,,,,
foodOmeletDesc,items,Food,A great way to start out your day of looting rotten homes and burnt out stores.,,,,,
foodGrillPork,items,Food,Grilled Pork,,,,,
foodGrillChicken,items,Food,Grilled Chicken,,,,,
foodSoupMushroom,items,Food,Mushroom Soup,,,,,
foodGrilledCorn,items,Food,Grilled Corn,,,,,
foodGrilledMushrooms,items,Food,Grilled Mushrooms,,,,,
foodGrilledPotato,items,Food,Grilled Potato,,,,,

foodBaconMacCheese,items,Food,Bacon Mac and Cheese,,,,,
foodBaconMacCheeseDesc,items,Food,Macaroni and cheese was a childhood favorite of many.,,,,,
foodHoagie,items,Food,East Coast Hoagie,,,,,
foodBLT,items,Food,BLT,,,,,
foodSausagePeppers,items,Food,Sausage and Peppers Hero,,,,,
foodMeatballSub,items,Food,Meatball Sub,,,,,
foodMeatballSubDesc,items,Food,Giovanni made it just for you. Are you going to complain?,,,,,
specialtySandwichDesc,items,Food,Precision crafted specialty sandwiches provide the best nutrition and offer a taste of regional bliss.,,,,,
picklesDesc,items,Food,Some among us value the pickle above twine. Be very wary of those individuals.,,,,,
coleSlawDesc,items,Food,A delicious treat,,,,,
foodRawPasta,items,Food,Raw Pasta,,,,,
foodRawPastaDesc,items,Food,You are on your way to being your own Lagasse,,,,,

foodSnoBalls,items,Food,Sno Balls,,,,,
foodTwinkies,items,Food,Twinkies,,,,,
foodCheezIts,items,Food,CheezIts,,,,,
foodBagChipsHotDesc,items,Food,Hot Bag of Chips,,,,,
foodBagChipsDesc,items,Food,Bag of Chips,,,,,
foodShortyBarDesc,items,Food,Shorty Bar,,,,,
foodChocBarDesc,items,Food,Chocolate Bar,,,,,
foodHoneyGlazedHam,items,Food,Honey Glazed Ham,,,,,
foodHoneyGlazedHamDesc,items,Food,Glazed with the amazing power of antibiotic. Maybe.,,,,,
foodFreshKobeBeef,items,Food,Kobe Beef,,,,,
foodFreshButter,items,Food,Butter,,,,,
foodFreshBaconMeatloaf,items,Food,Bacon Meatloaf,,,,,
foodFreshMilk,items,Food,Milk,,,,,
foodFreshBakedChickenBreast,items,Food,Baked Chicken Breast,,,,,
foodSpicyHotWings,items,Food,Spicy Hot Wings,,,,,
foodFreshHoneyGlazedHam,items,Food,Honey Glazed Ham,,,,,
foodBread,items,Food,Bread,,,,,
foodBreadDesc,items,Food,Another easy to produce vital survival food. Use it to make a number of dishes and sandwiches.,,,,,
foodSnoBallsDesc,items,Food,Sno Balls? Where's the fucking Twinkies? Sno Balls?,,,,,
foodCheezItsDesc,items,Food,The perfect blend of cracker and cheese. Perfect for snacking and heavy enough to remind people you mean business when thrown.,,,,,
foodTwinkiesDesc,items,Food,"Seems like you made it here before Tallahassee did so enjoy the spongy, yellow, delicious bastards.",,,,,
foodBagChipsHotDesc,items,Food,A bag of crispy salted potato chips. They're hot and best eaten with a drink at the ready.,,,,,
foodBagChipsDesc,items,Food,A bag of crispy salted potato chips.,,,,,
foodShortyBarDesc,items,Food,Eating this bar will give you the power of Mighty Mouse!,,,,,
foodChocBarDesc,items,Food,Diabeetus what diabeetus. This is the apocalypse so have at it and shove that candy in your mouth like there is no tomorrow.,,,,,
foodGrilledCornDesc,items,Food,Grilling your vegetables is a great way to make a quick nutritious snack.,,,,,
foodGrilledMushroomsDesc,items,Food,Grilling your vegetables is a great way to make a quick nutritious snack.,,,,,
foodGrilledPotatoDesc,items,Food,Grilling your vegetables is a great way to make a quick nutritious snack.,,,,,
foodVinegarDesc,items,Food,Vinegar is a very common ingredient for many recipes.,,,,,
foodMayonnaiseDesc,items,Food,Mayonnaise can be used to add the perfect finishing touch on sandwiches.,,,,,
foodBrisketDesc,items,Food,Smoking meats provides massive benefits to you mentally and physically. Take it back to the days before the apocalypse and when cookouts were your weekend getaway.,,,,,
foodPulledPorkDesc,items,Food,Smoking meats provides massive benefits to you mentally and physically. Take it back to the days before the apocalypse and when cookouts were your weekend getaway.,,,,,
foodBeerCanChickenDesc,items,Food,Smoking meats provides massive benefits to you mentally and physically. Take it back to the days before the apocalypse and when cookouts were your weekend getaway.,,,,,
foodLettuceAndTomatoRH,items,Food,Lettuce and Tomato,,,,,
foodLettuceAndTomatoRHDesc,items,Food,A classic combination for your sandwiches.,,,,,
foodWhiteMeatRH,items,Food,Raw White Meat,,,,,
foodWhiteMeatRHDesc,items,Food,"Meat can be eaten raw but is better charred, grilled, boiled or made into stews at a campfire.",,,,,
foodBoiledWhiteMeatRH,items,Food,Boiled White Meat,,,,,
foodBoiledWhiteMeatRHDesc,items,Food,"While not as yummy as grilled meat, boiling is an efficient way to prepare a meal.",,,,,
foodBoiledWhiteMeatRHSchematic,items,Food,Boiled White Meat Recipe,,,,,
foodGrilledWhiteMeatRH,items,Food,Grilled White Meat,,,,,
foodGrilledWhiteMeatRHDesc,items,Food,"Meat! Meat on fire! Good! Meat fresh off the grill is nutritious, delicious, and filling.",,,,,
foodGrilledWhiteMeatRHSchematic,items,Food,Grilled White Meat Recipe,,,,,
foodWhiteMeatStewRH,items,Food,White Meat Stew,,,,,
foodWhiteMeatStewRHDesc,items,Food,A survivor isn't worth his salt if he can't make a meat stew.,,,,,
foodWhiteMeatStewRHSchematic,items,Food,White Meat Stew Recipe,,,,,
foodCharredWhiteMeatRH,items,Food,Charred White Meat,,,,,
foodCharredWhiteMeatRHDesc,items,Food,Charring meat isn't the best way to cook it but it's much better than eating it raw.,,,,,
foodRedMeatRH,items,Food,Raw Red Meat,,,,,
foodRedMeatRHDesc,items,Food,"Meat can be eaten raw but is better charred, grilled, boiled or made into stews at a campfire.",,,,,
foodBoiledRedMeatRH,items,Food,Boiled Red Meat,,,,,
foodBoiledRedMeatRHDesc,items,Food,"While not as yummy as grilled meat, boiling is an efficient way to prepare a meal.",,,,,
foodBoiledRedMeatRHSchematic,items,Food,Boiled Red Meat Recipe,,,,,
foodGrilledRedMeatRH,items,Food,Grilled Red Meat,,,,,
foodGrilledRedMeatRHDesc,items,Food,"Meat! Meat on fire! Good! Meat fresh off the grill is nutritious, delicious, and filling.",,,,,
foodGrilledRedMeatRHSchematic,items,Food,Grilled Red Meat Recipe,,,,,
foodRedMeatStewRH,items,Food,Red Meat Stew,,,,,
foodRedMeatStewRHDesc,items,Food,A survivor isn't worth his salt if he can't make a meat stew.,,,,,
foodRedMeatStewRHSchematic,items,Food,Red Meat Stew Recipe,,,,,
foodCharredRedMeatRH,items,Food,Charred Red Meat,,,,,
foodCharredRedMeatRHDesc,items,Food,Charring meat isn't the best way to cook it but it's much better than eating it raw.,,,,,
foodTrailMixRH,items,Food,Survival Mix,,,,,
foodTrailMixRHDesc,items,Food,Survival Mix is a great survival food that can be easily made and provides you with a nutritious snack.,,,,,
foodJerkyRH,items,Food,Dried Jerky,,,,,
foodJerkyRHDesc,items,Food,Jerky is made from drying and curing meat and can give a great boost to your hunger.,,,,,
foodSaltRH,items,Food,Salt,,,,,
foodSaltRHDesc,items,Food,Salt is worth it's weight in gold. In old times it was a precious resource. Salt can be used to cure meats and preserve food.,,,,,
resourceSaltRH,items,Food,Salt Deposit,,,,,
resourceSaltRHDesc,items,Food,Mined salt deposits can be collected and boiled with clean water to make useable salt.,,,,,
foodButterRH,items,Food,Butter
foodButterRHDesc,items,Food,"Butter compliments every meal and is a key ingredient in larger dishes."
drinkFrozenIceeRH,items,Food,Icee,,,,,
drinkCanSodaRH,items,Food,Can of Soda,,,,,
drinkFrozenIceeRHDesc,items,Food,You remember these from a happier time. You can not believe this machine still works. Wonder if the water is still good in this. You can not resist. It's cherry! This helps you survive in severely warm environments.,,,,,
drinkCanSodaRHDesc,items,Food,The diabeetus was sure kicking your ass before all this happened but it's the end of the world as we know it anyway and you feel fine so.,,,,,
drinkYuccaJuiceSmoothieDesc,items,Food,This smoothie helps you survive in severely warm environments.,,,,,
resourceCropDriedChrysanthemumRH,items,Farming,Dried Chrysanthemum Leaves,,,,,
resourceCropDriedChrysanthemumRHDesc,items,Farming,Dried chrysanthemum leaves can be used to make red tea.,,,,,
resourceCropDriedGoldenrodRH,items,Farming,Dried Goldenrod Leaves,,,,,
resourceCropDriedGoldenrodRHDesc,items,Farming,Dried goldenrod leaves can be used to make goldenrod tea.,,,,,
drinkJarKoolAid,items,Food,Kook Aid,,,,,
drinkJarKoolAidDesc,items,Food,Nothing gets you busting through walls with energy better than a red drink!,,,,,
foodPieCrustRH,items,Food,Pie Crust,,,,,
foodPieCrustRHDesc,items,Food,The best crust you can make given the situation. Enjoy the flaky goodness.,,,,,
challenge_hotrod_title,quests,Quests,"H0tr0d's Request"
foodBeefTacos,items,Food,Beef Tacos
foodBeefTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodBeefTacosSchematic,items,Food,Beef Tacos Recipe
foodLambTacos,items,Food,Lamb Tacos
foodLambTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodLambTacosSchematic,items,Food,Lamb Tacos Recipe
foodTunaTacos,items,Food,Tuna Tacos
foodTunaTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodTunaTacosSchematic,items,Food,Tuna Tacos Recipe
foodShamTacos,items,Food,Sham Tacos
foodShamTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodShamTacosSchematic,items,Food,Sham Tacos Recipe
foodChickenTacos,items,Food,Chicken Tacos
foodChickenTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodChickenTacosSchematic,items,Food,Chicken Tacos Recipe
foodCatFoodTacos,items,Food,Cat Food Tacos
foodCatFoodTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodCatFoodTacosSchematic,items,Food,Cat Food Tacos Recipe
foodDogFoodTacos,items,Food,Dog Food Tacos
foodDogFoodTacosDesc,items,Food,Who wants to live off the land when it's taco Tuesday? Or is this Thursday?
foodDogFoodTacosSchematic,items,Food,Dog Food Tacos Recipe
foodMRERH,items,Food,MRE,,,,,
foodMRERHDesc,items,Food,A Military grade meal that will go a long way in nourishing you. This comes with no food poisoning chances. Find more in military loot..,,,,,
foodDentedCanBeef,items,Food,Dented Can of Large Beef Ration
foodDentedCanCatfood,items,Food,Dented Can of Cat Food
foodDentedCanChicken,items,Food,Dented Can of Chicken Ration
foodDentedCanChili,items,Food,Dented Can of Chili
foodDentedCanDogfood,items,Food,Dented Can of Dog Food
foodDentedCanLamb,items,Food,Dented Can of Lamb Rations
foodDentedCanMiso,items,Food,Dented Can of Miso
foodDentedCanPasta,items,Food,Dented Can of Pasta
foodDentedCanPears,items,Food,Dented Can of Pears
foodDentedCanPeas,items,Food,Dented Can of Peas
foodDentedCanSalmon,items,Food,Dented Can of Salmon
foodDentedCanSham,items,Food,Dented Can of Sham
foodDentedCanSoup,items,Food,Dented Can of Chicken Soup
foodDentedCanStock,items,Food,Dented Can of Stock
foodDentedCanTuna,items,Food,Dented Can of Tuna
foodDentedCanDesc,items,Food,A dented can runs the risk of having gone bad and giving you dysentery.