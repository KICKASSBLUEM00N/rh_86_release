Key,Source,Context,EnglishFrench,Latam,German,Klingon,Spanish,Polish
vehicleFlatBoat,vehicles,vehicle,new,FlatBottom Boat,,Barco de fondo plano,,,Barco de fondo plano,
vehicleFlatBoatPlaceable,vehicles,item,Flat Bottom Boat Placeable,,Barco de fondo plano colocable,,,Barco de fondo plano colocable,
vehicleFlatBoatPlaceableDesc,vehicles,item, Swim if you like ? ,,¿Nada si quieres?,,,¿Nada si quieres?,
vehicleDinghy,vehicles,vehicle,new,Dinghy,,Bote inflable,,,Bote inflable,
vehicleDinghyPlaceable,vehicles,item,A Dinghy Placeable,,Un bote inflable colocable,,,Un bote inflable colocable,
vehicleDinghyPlaceableDesc,vehicles,item, Fast boat but swim if you like ? ,,Bote rápido pero ¿nada si quieres?,,,Bote rápido pero ¿nada si quieres?,