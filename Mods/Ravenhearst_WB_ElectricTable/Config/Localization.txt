﻿Key,Source,Context,English
electricTableRH,blocks,Workstation,Electrician Table,,,,,
electricTableRHDesc,blocks,Workstation,"The electrician table is used to construct electrical items such as doors and lights.",,,,,
electricTableRHSchematic,blocks,Workstation,Electrician Table Schematic,,,,,
cntElectricTableBustedRandomLootHelperRH,blocks,Container,= Destroyed Electrician Table = Random Helper,,,,,
cntElectricTableBustedRandomLootHelperRHDesc,blocks,Container,Can spawn an electrician table.  10% chance for a working one.,,,,,
cntCollapsedElectricTableRH,blocks,Container,Destroyed Electrician Table,,,,,